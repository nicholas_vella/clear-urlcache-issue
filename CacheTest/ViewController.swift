//
//  ViewController.swift
//  CacheTest
//
//  Created by Nicholas Vella on 10/9/19.
//  Copyright © 2019 Seventh Beam. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func testCache(_ sender: Any) {
        let url = URL(string: "https://eml-beta.azure-api.net/test/cache?subscription-key=920ef9397c5849dab85cffabac9ca80a")!
        let urlReuqest = URLRequest(url: url)
        
        if let cacheData = URLCache.shared.cachedResponse(for: urlReuqest) {
            let string = String(data: cacheData.data, encoding: .utf8)!
            debugPrint("✅ Cache Existing - \(string)")
        } else {
            debugPrint("🛑 No Cache")
        }
        
        let task = URLSession.shared.dataTask(with: urlReuqest) {(data, response, error) in
        }
        
        task.resume()
    }
    
    @IBAction func clearCache(_ sender: Any) {
        URLCache.shared.removeAllCachedResponses()
        debugPrint("🗑 Cache Cleared")
    }
}

